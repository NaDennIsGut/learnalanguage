-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 15, 2019 at 12:22 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbsystems`
--

-- --------------------------------------------------------

--
-- Table structure for table `Activation`
--

CREATE TABLE `Activation` (
  `ID` int(11) NOT NULL,
  `ActCode` varchar(20) NOT NULL,
  `Cdate` date NOT NULL,
  `mail` varchar(100) NOT NULL,
  `Act` enum('no','yes') NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` int(20) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(30) DEFAULT NULL,
  `price` float NOT NULL,
  `shipping` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `name`, `description`, `picture`, `price`, `shipping`) VALUES
(1, 'Art-Socks', 'Socks with a pieace of art', 'images/artsocks.jpeg', 120, 5),
(2, 'Christmas Socks', 'Socks to show your mood in winter', 'images/christmassocks.jpeg', 120, 5),
(3, 'Diamond Socks', 'Socks that go for formal and casual', 'images/diamondsocks.jpeg', 120, 5),
(4, 'Dinosaur Socks', 'Socks to show your love for dinos', 'images/dinosocks.jpeg', 120, 5),
(5, 'Doctor Socks', 'Socks to show your are of work', 'images/doctorsocks.jpeg', 120, 5),
(6, 'Dog Socks', 'Socks that have dogs', 'images/dogsocks.jpeg', 120, 5),
(7, 'Dolphin Socks', 'Socks with dolphins', 'images/dolphinsocks.jpeg', 120, 5),
(8, 'Ice Cream Socks', 'Socks that show ice cream', 'images/icecreamsocks.jpeg', 120, 5),
(9, 'New York Socks', 'Socks with the logo of NY', 'images/nysocks.jpeg', 120, 5),
(10, 'Peruvian Socks', 'Socks with a Peruvian design', 'images/Peruviansocks.jpeg', 120, 5),
(11, 'Puzzle Socks', 'Socks that reperesent a puzzle', 'images/puzzlesocks.jpeg', 120, 5),
(12, 'Strip Socks', 'Socks for a casual day', 'images/stripsocks.jpeg', 120, 5),
(13, 'Taco Socks', 'Socks for the Mexicans', 'images/tacossocks.jpeg', 120, 5),
(14, 'Trump Socks', 'Socks with that face', 'images/trumpsocks.jpeg', 120, 5);

-- --------------------------------------------------------

--
-- Table structure for table `Clients`
--

CREATE TABLE `Clients` (
  `ID` int(11) NOT NULL,
  `ActCode` varchar(10) NOT NULL,
  `Cdate` datetime NOT NULL,
  `Mail` varchar(50) NOT NULL,
  `Act` enum('no','yes') NOT NULL DEFAULT 'yes',
  `Name` varchar(50) NOT NULL,
  `PostalC` int(5) NOT NULL,
  `City` varchar(50) NOT NULL,
  `Address` varchar(50) NOT NULL,
  `Password` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Clients`
--

INSERT INTO `Clients` (`ID`, `ActCode`, `Cdate`, `Mail`, `Act`, `Name`, `PostalC`, `City`, `Address`, `Password`) VALUES
(13, '24434051', '2019-01-08 19:41:21', 'diegoferrdz@hotmail.com', 'no', 'Diego', 64634, 'Monterrey', 'Manuel Garcia Morente 159', '93279e3308bdbbeed946fc965017f67a'),
(14, '92711362', '2019-01-10 14:09:42', '', 'no', 'fasdf', 0, '', 'asfda', 'd41d8cd98f00b204e9800998ecf8427e'),
(15, '26257537', '2019-01-10 14:10:56', '', 'no', 'fasdf', 0, '', 'asfda', 'd41d8cd98f00b204e9800998ecf8427e');

-- --------------------------------------------------------

--
-- Table structure for table `ClientsOn`
--

CREATE TABLE `ClientsOn` (
  `unuser` varchar(40) CHARACTER SET utf8 NOT NULL,
  `utime` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ClientsOn`
--

INSERT INTO `ClientsOn` (`unuser`, `utime`) VALUES
('Diego', 1547551315);

-- --------------------------------------------------------

--
-- Table structure for table `CurrentUser`
--

CREATE TABLE `CurrentUser` (
  `Currentuser` varchar(50) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Language`
--

CREATE TABLE `Language` (
  `id` int(11) NOT NULL,
  `symbol` varchar(10) NOT NULL,
  `language` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Language`
--

INSERT INTO `Language` (`id`, `symbol`, `language`) VALUES
(1, 'DE', 'German'),
(2, 'EN', 'English'),
(3, 'SP', 'Spanish'),
(4, 'FR', 'French'),
(5, 'IT', 'Italian'),
(6, 'DA', 'Danish'),
(7, 'DE', 'German'),
(8, 'EN', 'English'),
(9, 'SP', 'Spanish'),
(10, 'FR', 'French'),
(11, 'IT', 'Italian'),
(12, 'DA', 'Danish');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(20) NOT NULL,
  `title` varchar(50) NOT NULL,
  `message` varchar(200) NOT NULL,
  `answer` varchar(20) NOT NULL DEFAULT 'Not Answer',
  `language` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `title`, `message`, `answer`, `language`) VALUES
(1, 'First Question', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n                	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n                	quis nostrud e', 'Not Answer', 'German'),
(2, 'Second Question', 'Test', 'Not Answer', 'Spanish'),
(3, 'Third Question', 'Test', 'Not Answer', 'English'),
(4, 'Prueba', 'dafadsfas', 'Not Answer', NULL),
(5, 'Prueba', 'dafadsfas', 'Not Answer', NULL),
(6, '1', '1231231', 'Not Answer', NULL),
(7, '1', '1231231', 'Not Answer', NULL),
(8, '1', '1231231', 'Not Answer', NULL),
(9, '1', '1231231', 'Not Answer', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Activation`
--
ALTER TABLE `Activation`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Clients`
--
ALTER TABLE `Clients`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ClientsOn`
--
ALTER TABLE `ClientsOn`
  ADD PRIMARY KEY (`unuser`);

--
-- Indexes for table `CurrentUser`
--
ALTER TABLE `CurrentUser`
  ADD PRIMARY KEY (`Currentuser`);

--
-- Indexes for table `Language`
--
ALTER TABLE `Language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Activation`
--
ALTER TABLE `Activation`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Clients`
--
ALTER TABLE `Clients`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `Language`
--
ALTER TABLE `Language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
