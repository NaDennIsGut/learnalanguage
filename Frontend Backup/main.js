var filter= [];
var allQuestions = [];
var randomColor = Math.floor(Math.random()*16777215).toString(16);

function addLanguage(){
    let choosenLanguage = document.getElementById("languageSelector").value;
    if(choosenLanguage=="Choose a Language..."){
        window.alert("Please choose a Language");
    }else if(!filter.includes(choosenLanguage)){
        filter.push(choosenLanguage);
        updateFilter();
    }
}

function updateFilter(){
    //delete old Filter
    let oldFilterElements = document.getElementsByClassName("filterElement");
    while(oldFilterElements.length>0){
        oldFilterElements[0].parentNode.removeChild(oldFilterElements[0]);
    }
    //build actual Filter
    for(let i=0; i <filter.length;i++){

        let filterElement = document.createElement('div');
        filterElement.className="filterElement";
        filterElement.setAttribute("id",filter[i]);

        let p = document.createElement('p');
        let textnode = document.createTextNode(filter[i]);
        p.appendChild(textnode);

        //init delete Button
        let deleteButton = document.createElement('button')
        let deleteText= document.createTextNode("Delete");
        deleteButton.appendChild(deleteText);
        deleteButton.id = i;
        deleteButton.setAttribute("id","deleteButtonID");
        //onclick trigger delete function - deleteButton itself is passed
        deleteButton.onclick = function(){
            deleteFilterElement(deleteButton);
        }
        // window.alert(filterElement);

        filterElement.appendChild(p);
        filterElement.appendChild(deleteButton);
        document.getElementById("filter").appendChild(filterElement);
    }

}
    function deleteFilterElement(clickedDeleteButton)
    {
        clickedDeleteButton.parentNode.parentNode.removeChild(clickedDeleteButton.parentNode);
        filter.splice(clickedDeleteButton.id, 1);

    }

    function openSendQuestionDialogue()
    {
        document.getElementById("sendQuestionDialog").style.display = 'block';
        var questionText = document.getElementById("askTextarea").value;
        document.getElementById("askTextarea").value="";
        document.getElementById("questionText").value = questionText;

    }

    function closeSendQuestionDialogue()
    {
        document.getElementById("sendQuestionDialog").style.display = 'none';
        document.getElementById("idWindow").style.display = 'none';
    }

    function reallySendQuestion(){
        document.getElementById("idWindow").style.display = 'block';
        document.getElementById("idTextField").innerHTML ="#" + (allQuestions.length + 1);
    }

    function copyID(){
        var copyText = document.getElementById("idTextField");
        console.log(copyText.value);
        copyText.select();
        document.execCommand("copy");
    }

    function acceptIdAndSendToDB(){
        //create class div
        let question = document.createElement('div');
        question.className="question";
        question.setAttribute("id", "question");

        //create button to reply
        var replyButton = document.createElement("button");
        replyButton.innerHTML = "Reply";
        replyButton.onclick = function(){
            openRespondToQuestionArea(replyButton.parentNode.childNodes[0].innerHTML);
        }
        replyButton.setAttribute("id","replyButtonID");
        //create p element that displays question text
        let p = document.createElement('p');
        p.innerHTML = questionText.value;


        //create the p element that stores the id but is not displayed (or maybe it should be?)
        let id = document.createElement('p');
        id.innerHTML = "#" + (allQuestions.length + 1);
        id.style.display = 'none';


        //append p  to question and appent question to question area
        question.appendChild(p);
        question.appendChild(replyButton);
        question.appendChild(id);
        document.getElementById('questionArea').appendChild(question);

        //do not change this style
        document.getElementById("sendQuestionDialog").style.display = 'none';
        document.getElementById("idWindow").style.display = 'none';
        allQuestions.push(question);


    }

    function openRespondToQuestionArea(questionText){
        document.getElementById('closeButtonX').style.display ='block';
        document.getElementById('respondToQuestionArea').style.width ='30%';
        document.getElementById('respondeToQuestionTextField').style.display = 'block';
        document.getElementById('questionInResponseArea').innerHTML = questionText;
        document.getElementById('questionInResponseArea').style.display = 'block';
        document.getElementById('sendResponseButton').style.display = 'block';
        document.getElementById('allResponsesInResponseArea').style.display = 'block';
        }

    function closeResponseArea(){
        document.getElementById('respondToQuestionArea').style.width ='0%';
        document.getElementById('respondeToQuestionTextField').style.display = 'none';
        document.getElementById('closeButtonX').style.display ='none';
        document.getElementById('questionInResponseArea').style.display = 'none';
        document.getElementById('sendResponseButton').style.display = 'none';
        document.getElementById('allResponsesInResponseArea').style.display = 'none';


    }

    function sendResponse(){
        let response = document.createElement('p');
        var responseText = document.getElementById('respondeToQuestionTextField').value;
        response.innerHTML = responseText;
        document.getElementById('allResponsesInResponseArea').appendChild(response);
        document.getElementById('respondeToQuestionTextField').value = '';
    }

    function searchForQuestion(){
        //the id of each question is --> question.childNodes[2]

        for (let index = 0; index < allQuestions.length; index++)
        {
            if(document.getElementById('idSearchBar').value == allQuestions[index].childNodes[2].innerHTML)
            {
                openRespondToQuestionArea(allQuestions[index].childNodes[0].innerHTML);
            }


        }


    }
